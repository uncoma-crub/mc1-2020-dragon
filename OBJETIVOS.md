### Dragon: una nave espacial de carga reutilizable, diseño, especificaciones y propósito.

Realizar un trabajo de investigación conjunto en se que se haga un abordaje completo de los siguientes temas:

* Descripción del módulo Dragon, versiones y propósitos.
* Especificaciones del Dragon.
* Aspectos del diseño: Escudo térmico y Sistema de control de reacción, potencia y recuperación.
* Vinculación con la Estación Espacial Internacional: aspectos más importantes e hitos.
* Misión de servicios de reabastecimiento comercial: CRS1 a 6
* Descripción de Programa de tripulación de la NASA.
* Descripción del DragonLab
* Evaluación de las potenciales demandas de ingeniería en el desarrollo de Dragon y qué materias/conocimientos asociados en su carrera podrían ajustarse más a las mismas.
* Citar la bibliografía

Se sugiere la siguiente bibliografía para comenzar

(spaceX)[https://www.spacex.com/]

(wikipedia: Dragon - SpaceX)[https://en.wikipedia.org/wiki/SpaceX_Dragon]

(Seedhouse, Erik. SpaceX's Dragon: America's Next Generation Spacecraft. Springer, 2015.)[]

(SpaceX: General Falcon and Dragon discussion)[https://forum.nasaspaceflight.com/index.php?topic=29476.620]
